package cn.myzf.netty.client;

import cn.myzf.netty.client.initializer.ClientStart;
import javafx.scene.Parent;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;


/**
 *  服务器启动类
  * @Author: myzf
  * @Date: 2019/2/23 13:00
  * @param
*/
@SpringBootApplication
@PropertySource(value= "classpath:/application.properties")
/*因为这个包不在默认的springboot当前目录下，需要扫描，SpringBeanFactory才可以加载到context*/
@ComponentScan(basePackages = {"cn.myzf.common.util"})
public class ClientApplication {

	public static void main(String[] args) throws Exception {

		SpringApplicationBuilder springApplicationBuilder = new SpringApplicationBuilder()
				.sources(Parent.class)
				.child(ClientApplication.class);

		//springApplicationBuilder.application().addInitializers(new ClientStart());

		ConfigurableApplicationContext context = springApplicationBuilder.run(args);
		//等到springApplicationBuilder启动后再启动ClientStar(),要不然会报空指针异常
		new ClientStart().initialize(context);

	}

}